<?php

declare(strict_types=1);

namespace Tests\ObjectBox;

use PHPUnit\Framework\TestCase;
use Veroxis\Typedaccessor\ObjectBox;
use Veroxis\Typedaccessor\Exceptions\MismatchedType;
use Veroxis\Typedaccessor\Exceptions\PathDoesNotExist;
use Veroxis\Typedaccessor\Exceptions\PathTooShort;

final class ObjectBoxStringsTest extends TestCase
{
    public function testSetStringSuccess(): void
    {
        $box = ObjectBox::new();
        $success = $box->setString(['foo'], 'bar');
        $this->assertTrue($success);
        $this->assertSame('bar', $box->asString('foo'));
    }

    public function testSetStringFail(): void
    {
        $box = ObjectBox::new();
        $success = $box->setString(['foo', 'bar'], 'baz');
        $this->assertFalse($success);
        $this->assertNull($box->asString('foo', 'bar'));
    }

    public function testTrySetStringSuccess(): void
    {
        $box = ObjectBox::new();
        $box->trySetString(['foo'], 'bar');
        $this->assertSame('bar', $box->asString('foo'));
    }

    public function testTrySetStringFailPathDoesNotExist(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->trySetString(['foo', 'bar'], 'baz');
    }

    public function testTrySetStringFailPathTooShort(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathTooShort::class);
        $box->trySetString([], 'baz');
    }

    public function testAsStringAccessors(): void
    {
        $box = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ]);
        $this->assertTrue($box->asString('foo') === 'bar');
        $this->assertNull($box->asString('null'));
        $this->assertNull($box->asString('int'));
        $this->assertNull($box->asString('float'));
        $this->assertNull($box->asString('bool'));
        $this->assertNull($box->asString('array'));
        $this->assertNull($box->asString('object'));
        $this->assertNull($box->asString('non_existent'));
    }

    public function testTryAsStringString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->assertTrue($box->tryAsString('foo') === 'bar');
    }

    public function testTryAsStringNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->expectException(MismatchedType::class);
        $box->tryAsString('foo');
    }

    public function testTryAsStringInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->expectException(MismatchedType::class);
        $box->tryAsString('foo');
    }

    public function testTryAsStringFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->expectException(MismatchedType::class);
        $box->tryAsString('foo');
    }

    public function testTryAsStringBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->expectException(MismatchedType::class);
        $box->tryAsString('foo');
    }

    public function testTryAsStringArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->expectException(MismatchedType::class);
        $box->tryAsString('foo');
    }

    public function testTryAsStringObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->expectException(MismatchedType::class);
        $box->tryAsString('foo');
    }

    public function testTryAsStringNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->tryAsString('foo');
    }

    public function testIsStringString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->assertTrue($box->isString('foo'));
    }

    public function testIsStringNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->assertFalse($box->isString('foo'));
    }

    public function testIsStringInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->assertFalse($box->isString('foo'));
    }

    public function testIsStringFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->assertFalse($box->isString('foo'));
    }

    public function testIsStringBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->assertFalse($box->isString('foo'));
    }

    public function testIsStringArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->assertFalse($box->isString('foo'));
    }

    public function testIsStringObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->assertFalse($box->isString('foo'));
    }

    public function testIsStringNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->assertFalse($box->isString('foo'));
    }
}
