<?php

declare(strict_types=1);

namespace Tests\ObjectBox;

use PHPUnit\Framework\TestCase;
use Veroxis\Typedaccessor\ObjectBox;
use Veroxis\Typedaccessor\Exceptions\PathDoesNotExist;
use Veroxis\Typedaccessor\Exceptions\PathTooShort;

final class ObjectBoxRawTest extends TestCase
{
    public function testSetRawSuccess(): void
    {
        $box = ObjectBox::new();
        $success = $box->setRaw(['foo'], []);
        $this->assertTrue($success);
        $this->assertSame([], $box->asRaw('foo'));
    }

    public function testSetRawFail(): void
    {
        $box = ObjectBox::new();
        $success = $box->setRaw(['foo', 'bar'], []);
        $this->assertFalse($success);
        $this->assertNull($box->asRaw('foo', 'bar'));
    }

    public function testTrySetRawSuccess(): void
    {
        $box = ObjectBox::new();
        $box->trySetRaw(['foo'], []);
        $this->assertSame([], $box->asRaw('foo'));
    }

    public function testTrySetRawFailPathDoesNotExist(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->trySetRaw(['foo', 'bar'], []);
    }

    public function testTrySetRawFailPathTooShort(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathTooShort::class);
        $box->trySetRaw([], []);
    }

    public function testAsRawAccessors(): void
    {
        $box = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ]);
        $this->assertSame('bar', $box->asRaw('foo'));
        $this->assertNull($box->asRaw('null'));
        $this->assertSame(42, $box->asRaw('int'));
        $this->assertSame(3.1415926535, $box->asRaw('float'));
        $this->assertTrue($box->asRaw('bool'));
        $this->assertSame([], $box->asRaw('array'));
        $this->assertJsonStringEqualsJsonString(
            json_encode($box->asRaw('object'), JSON_THROW_ON_ERROR),
            json_encode((object) [], JSON_THROW_ON_ERROR)
        );
        $this->assertNull($box->asRaw('non_existent'));
    }

    public function testTryAsRawString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->assertSame('bar', $box->tryAsRaw('foo'));
    }

    public function testTryAsRawNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->assertNull($box->tryAsRaw('foo'));
    }

    public function testTryAsRawInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->assertSame(42, $box->tryAsRaw('foo'));
    }

    public function testTryAsRawFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->assertSame(3.1415926535, $box->tryAsRaw('foo'));
    }

    public function testTryAsRawBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->assertTrue($box->tryAsRaw('foo'));
    }

    public function testTryAsRawArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->assertSame([], $box->tryAsRaw('foo'));
    }

    public function testTryAsRawObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->assertJsonStringEqualsJsonString(
            json_encode($box->asRaw('foo'), JSON_THROW_ON_ERROR),
            json_encode((object) [], JSON_THROW_ON_ERROR)
        );
    }

    public function testTryAsRawNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->tryAsRaw('foo');
    }
}
