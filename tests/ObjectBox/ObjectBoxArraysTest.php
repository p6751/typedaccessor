<?php

declare(strict_types=1);

namespace Tests\ObjectBox;

use PHPUnit\Framework\TestCase;
use Veroxis\Typedaccessor\ObjectBox;
use Veroxis\Typedaccessor\Exceptions\PathDoesNotExist;
use Veroxis\Typedaccessor\Exceptions\PathTooShort;
use Veroxis\Typedaccessor\Exceptions\MismatchedType;

final class ObjectBoxArraysTest extends TestCase
{
    public function testSetArraySuccess(): void
    {
        $box = ObjectBox::new();
        $success = $box->setArray(['foo'], []);
        $this->assertTrue($success);
        $this->assertSame([], $box->asArray('foo'));
        $success = $box->setArray(['foo', 'bar'], []);
        $this->assertTrue($success);
        $this->assertSame([], $box->asArray('foo', 'bar'));
        $success = $box->setArray(['foo', 'bar', 'baz'], []);
        $this->assertTrue($success);
        $this->assertSame([], $box->asArray('foo', 'bar', 'baz'));
    }

    public function testSetArrayFail(): void
    {
        $box = ObjectBox::new();
        $success = $box->setArray(['foo', 'bar'], []);
        $this->assertFalse($success);
        $this->assertNull($box->asArray('foo', 'bar'));
    }

    public function testTrySetArraySuccess(): void
    {
        $box = ObjectBox::new();
        $box->trySetArray(['foo'], []);
        $this->assertSame([], $box->asArray('foo'));
    }

    public function testTrySetArrayFailPathDoesNotExist(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->trySetArray(['foo', 'bar'], []);
    }

    public function testTrySetArrayFailPathTooShort(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathTooShort::class);
        $box->trySetArray([], []);
    }

    public function testAsArrayAccessors(): void
    {
        $box = ObjectBox::from([
            'foo' => 'bar',
            'null' => null,
            'int' => 42,
            'float' => 3.1415926535,
            'bool' => true,
            'array' => [],
            'object' => (object) [],
        ]);
        $this->assertNull($box->asArray('foo'));
        $this->assertNull($box->asArray('null'));
        $this->assertNull($box->asArray('int'));
        $this->assertNull($box->asArray('float'));
        $this->assertNull($box->asArray('bool'));
        $this->assertSame([], $box->asArray('array'));
        $this->assertNull($box->asArray('object'));
        $this->assertNull($box->asArray('non_existent'));
    }

    public function testTryAsArrayString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->expectException(MismatchedType::class);
        $box->tryAsArray('foo');
    }

    public function testTryAsArrayNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->expectException(MismatchedType::class);
        $box->tryAsArray('foo');
    }

    public function testTryAsArrayInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->expectException(MismatchedType::class);
        $box->tryAsArray('foo');
    }

    public function testTryAsArrayFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->expectException(MismatchedType::class);
        $box->tryAsArray('foo');
    }

    public function testTryAsArrayBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->expectException(MismatchedType::class);
        $box->tryAsArray('foo');
    }

    public function testTryAsArrayArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->assertSame([], $box->tryAsArray('foo'));
    }

    public function testTryAsArrayObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->expectException(MismatchedType::class);
        $box->tryAsArray('foo');
    }

    public function testTryAsArrayNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->expectException(PathDoesNotExist::class);
        $box->tryAsArray('foo');
    }

    public function testIsArrayString(): void
    {
        $box = ObjectBox::from(['foo' => 'bar']);
        $this->assertFalse($box->isArray('foo'));
    }

    public function testIsArrayNull(): void
    {
        $box = ObjectBox::from(['foo' => null]);
        $this->assertFalse($box->isArray('foo'));
    }

    public function testIsArrayInt(): void
    {
        $box = ObjectBox::from(['foo' => 42]);
        $this->assertFalse($box->isArray('foo'));
    }

    public function testIsArrayFloat(): void
    {
        $box = ObjectBox::from(['foo' => 3.1415926535]);
        $this->assertFalse($box->isArray('foo'));
    }

    public function testIsArrayBool(): void
    {
        $box = ObjectBox::from(['foo' => true]);
        $this->assertFalse($box->isArray('foo'));
    }

    public function testIsArrayArray(): void
    {
        $box = ObjectBox::from(['foo' => []]);
        $this->assertTrue($box->isArray('foo'));
    }

    public function testIsArrayObject(): void
    {
        $box = ObjectBox::from(['foo' => (object) []]);
        $this->assertFalse($box->isArray('foo'));
    }

    public function testIsArrayNonExistent(): void
    {
        $box = ObjectBox::new();
        $this->assertFalse($box->isArray('foo'));
    }
}
