<?php

declare(strict_types=1);

namespace Veroxis\Typedaccessor;

use JsonException;
use Stringable;
use Veroxis\Typedaccessor\Exceptions\PathDoesNotExist;
use Veroxis\Typedaccessor\Exceptions\PathTooShort;
use Veroxis\Typedaccessor\Exceptions\MismatchedType;
use Veroxis\Typedaccessor\Json;

class ObjectBox implements TypedAccessor, Stringable
{
    // constants

    protected const TYPE_BOOL = 'bool';
    protected const TYPE_INT = 'int';
    protected const TYPE_FLOAT = 'float';
    protected const TYPE_STRING = 'string';
    protected const TYPE_ARRAY = 'array';
    protected const TYPE_OBJECT = 'object';
    protected const TYPE_NULL = 'null';

    // properties

    /** @var mixed[]|object */
    protected array|object $data;

    // methods::public

    public static function new(): self
    {
        return self::from((object) []);
    }

    /**
     * @param mixed[]|object $data
     */
    public static function from(array|object $data): self
    {
        $box = new self();
        $box->data = $data;
        return $box;
    }

    /**
     * @throws JsonException
     */
    public static function fromJson(string $json): self
    {
        return self::from(Json::decode($json));
    }

    /**
     * @throws JsonException
     */
    public function toJson(): string
    {
        return Json::encode($this->unwrap());
    }

    /**
     * @return mixed[]|object
     */
    public function unwrap(): array|object
    {
        return $this->data;
    }

    // Interface: Stringable

    /**
     * @throws JsonException
     */
    public function __toString(): string
    {
        return $this->toJson();
    }

    // Interface: TypedAccessor

    public function exists(string|int ...$path): bool
    {
        try {
            $this->atKey(...$path);
            return true;
        } catch (PathDoesNotExist) {
            return false;
        }
    }

    public function delete(string|int ...$path): bool
    {
        try {
            /** @var string|int|null $key */
            $key = array_pop($path);
            if (is_null($key)) {
                PathDoesNotExist::throw(...$path);
            }
            /** @var mixed $data */
            $data = &$this->atKey(...$path);
            if (self::canAccessProperty($data, $key)) {
                /** @var object $data */
                unset($data->$key);
                return true;
            }
            if (self::canAccessKey($data, $key)) {
                /** @var array<string|int, mixed> $data */
                unset($data[$key]);
                return true;
            }
        } catch (PathDoesNotExist) {
        }
        return false;
    }

    public function isNull(string|int ...$path): bool
    {
        try {
            return is_null($this->atKey(...$path));
        } catch (PathDoesNotExist) {
            return false;
        }
    }

    public function asRaw(string|int ...$path): mixed
    {
        try {
            return $this->atKey(...$path);
        } catch (PathDoesNotExist) {
            return null;
        }
    }

    public function tryAsRaw(string|int ...$path): mixed
    {
        return $this->atKey(...$path);
    }

    public function setRaw(array $path, mixed $value): bool
    {
        try {
            $this->setAtKey($path, $value);
            return true;
        } catch (PathDoesNotExist | PathTooShort) {
            return false;
        }
    }

    public function trySetRaw(array $path, mixed $value): void
    {
        $this->setAtKey($path, $value);
    }

    public function isString(string|int ...$path): bool
    {
        return is_string($this->asRaw(...$path));
    }

    public function asString(string|int ...$path): ?string
    {
        $data = $this->asRaw(...$path);
        return is_string($data) ? $data : null;
    }

    public function tryAsString(string|int ...$path): string
    {
        $data = $this->atKey(...$path);
        return is_string($data) ? $data : MismatchedType::throw(self::TYPE_STRING, $data);
    }

    public function setString(array $path, string $value): bool
    {
        return $this->setRaw($path, $value);
    }

    public function trySetString(array $path, string $value): void
    {
        $this->setAtKey($path, $value);
    }

    public function isInt(string|int ...$path): bool
    {
        return is_int($this->asRaw(...$path));
    }

    public function asInt(string|int ...$path): ?int
    {
        $data = $this->asRaw(...$path);
        return is_int($data) ? $data : null;
    }

    public function tryAsInt(string|int ...$path): int
    {
        $data = $this->atKey(...$path);
        return is_int($data) ? $data : MismatchedType::throw(self::TYPE_INT, $data);
    }

    public function setInt(array $path, int $value): bool
    {
        return $this->setRaw($path, $value);
    }

    public function trySetInt(array $path, int $value): void
    {
        $this->setAtKey($path, $value);
    }

    public function isFloat(string|int ...$path): bool
    {
        return is_float($this->asRaw(...$path));
    }

    public function asFloat(string|int ...$path): ?float
    {
        $data = $this->asRaw(...$path);
        return is_float($data) ? $data : null;
    }

    public function tryAsFloat(string|int ...$path): float
    {
        $data = $this->atKey(...$path);
        return is_float($data) ? $data : MismatchedType::throw(self::TYPE_FLOAT, $data);
    }

    public function setFloat(array $path, float $value): bool
    {
        return $this->setRaw($path, $value);
    }

    public function trySetFloat(array $path, float $value): void
    {
        $this->setAtKey($path, $value);
    }

    public function isBool(string|int ...$path): bool
    {
        return is_bool($this->asRaw(...$path));
    }

    public function asBool(string|int ...$path): ?bool
    {
        $data = $this->asRaw(...$path);
        return is_bool($data) ? $data : null;
    }

    public function tryAsBool(string|int ...$path): bool
    {
        $data = $this->atKey(...$path);
        return is_bool($data) ? $data : MismatchedType::throw(self::TYPE_BOOL, $data);
    }

    public function setBool(array $path, bool $value): bool
    {
        return $this->setRaw($path, $value);
    }

    public function trySetBool(array $path, bool $value): void
    {
        $this->setAtKey($path, $value);
    }

    public function isArray(string|int ...$path): bool
    {
        return is_array($this->asRaw(...$path));
    }

    public function asArray(string|int ...$path): ?array
    {
        $data = $this->asRaw(...$path);
        return is_array($data) ? $data : null;
    }

    public function tryAsArray(string|int ...$path): array
    {
        $data = $this->atKey(...$path);
        return is_array($data) ? $data : MismatchedType::throw(self::TYPE_ARRAY, $data);
    }

    public function setArray(array $path, array $value): bool
    {
        return $this->setRaw($path, $value);
    }

    public function trySetArray(array $path, array $value): void
    {
        $this->setAtKey($path, $value);
    }

    public function isObject(string|int ...$path): bool
    {
        return is_object($this->asRaw(...$path));
    }

    public function asObject(string|int ...$path): ?object
    {
        $data = $this->asRaw(...$path);
        return is_object($data) ? $data : null;
    }

    public function tryAsObject(string|int ...$path): object
    {
        $data = $this->atKey(...$path);
        return is_object($data) ? $data : MismatchedType::throw(self::TYPE_OBJECT, $data);
    }

    public function setObject(array $path, object $value): bool
    {
        return $this->setRaw($path, $value);
    }

    public function trySetObject(array $path, object $value): void
    {
        $this->setAtKey($path, $value);
    }

    // methods::protected

    protected function __construct()
    {
    }

    protected static function canAccessProperty(mixed $data, string|int $idx): bool
    {
        return is_object($data) && property_exists($data, (string) $idx);
    }

    protected static function canAccessKey(mixed $data, string|int $idx): bool
    {
        return is_array($data) && array_key_exists($idx, $data);
    }

    /**
     * @param array<string|int> $path
     *
     * @throws PathDoesNotExist
     * @throws PathTooShort
     */
    protected function setAtKey(array $path, mixed $value): void
    {
        $key = array_pop($path);
        if (is_null($key)) {
            PathTooShort::throw();
        }
        $pointer = &$this->atKey(...$path);
        if (is_object($pointer)) {
            $pointer->$key = $value;
            return;
        }
        if (is_array($pointer)) {
            $pointer[$key] = $value;
            return;
        }
        PathDoesNotExist::throw(...$path);
    }

    /**
     * @throws PathDoesNotExist
     */
    protected function &atKey(string|int ...$path): mixed
    {
        $pointer = &$this->data;
        foreach ($path as $nextKey) {
            if (self::canAccessProperty($pointer, $nextKey)) {
                $pointer = &$pointer->$nextKey;
                continue;
            }
            if (self::canAccessKey($pointer, $nextKey)) {
                $pointer = &$pointer[$nextKey];
                continue;
            }
            PathDoesNotExist::throw(...$path);
        }
        return $pointer;
    }
}
