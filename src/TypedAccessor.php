<?php

declare(strict_types=1);

namespace Veroxis\Typedaccessor;

use Veroxis\Typedaccessor\Exceptions\PathDoesNotExist;
use Veroxis\Typedaccessor\Exceptions\PathTooShort;
use Veroxis\Typedaccessor\Exceptions\MismatchedType;

interface TypedAccessor
{
    /**
     * Check if the given path exists.
     */
    public function exists(string|int ...$path): bool;

    /**
     * Delete the value at the the given path.
     */
    public function delete(string|int ...$path): bool;

    /**
     * Check if the given path exists and is of type `null`.
     */
    public function isNull(string|int ...$path): bool;

    // -- Mixed --

    /**
     * Try to get the value as `mixed`.
     */
    public function asRaw(string|int ...$path): mixed;

    /**
     * Try to get the value as `mixed`.
     *
     * @throws PathDoesNotExist If any key within the path is not found.
     */
    public function tryAsRaw(string|int ...$path): mixed;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @return bool to indicate if the action was successful or not.
     */
    public function setRaw(array $path, mixed $value): bool;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @throws PathDoesNotExist To indicate that the given path does not exist.
     * @throws PathTooShort     If the given path is empty.
     */
    public function trySetRaw(array $path, mixed $value): void;

    // -- String --

    /**
     * Check if the given path exists and is of type `string`.
     */
    public function isString(string|int ...$path): bool;

    /**
     * Try to get the value as `string`. Return `null` if there is no `string`.
     */
    public function asString(string|int ...$path): ?string;

    /**
     * Try to get the value as `string`.
     *
     * @throws MismatchedType   If the path is found but is no `string`.
     * @throws PathDoesNotExist If any key within the path is not found.
     */
    public function tryAsString(string|int ...$path): string;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @return bool to indicate if the action was successful or not.
     */
    public function setString(array $path, string $value): bool;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @throws PathDoesNotExist To indicate that the given path does not exist.
     * @throws PathTooShort     If the given path is empty.
     */
    public function trySetString(array $path, string $value): void;

    // -- Int --

    /**
     * Check if the given path exists and is of type `int`.
     */
    public function isInt(string|int ...$path): bool;

    /**
     * Try to get the value as `int`. Return `null` if there is no `int`.
     */
    public function asInt(string|int ...$path): ?int;

    /**
     * Try to get the value as `int`.
     *
     * @throws MismatchedType   If the path is found but is no `float`.
     * @throws PathDoesNotExist If any key within the path is not found.
     */
    public function tryAsInt(string|int ...$path): int;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @return bool to indicate if the action was successful or not.
     */
    public function setInt(array $path, int $value): bool;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @throws PathDoesNotExist To indicate that the given path does not exist.
     * @throws PathTooShort     If the given path is empty.
     */
    public function trySetInt(array $path, int $value): void;

    // -- Float --

    /**
     * Check if the given path exists and is of type `float`.
     */
    public function isFloat(string|int ...$path): bool;

    /**
     * Try to get the value as `float`. Return `null` if there is no `float`.
     */
    public function asFloat(string|int ...$path): ?float;

    /**
     * Try to get the value as `float`.
     *
     * @throws MismatchedType   If the path is found but is no `float`.
     * @throws PathDoesNotExist If any key within the path is not found.
     */
    public function tryAsFloat(string|int ...$path): float;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @return bool to indicate if the action was successful or not.
     */
    public function setFloat(array $path, float $value): bool;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @throws PathDoesNotExist To indicate that the given path does not exist.
     * @throws PathTooShort     If the given path is empty.
     */
    public function trySetFloat(array $path, float $value): void;

    // -- Bool --

    /**
     * Check if the given path exists and is of type `bool`.
     */
    public function isBool(string|int ...$path): bool;

    /**
     * Try to get the value as `bool`. Return `null` if there is no `bool`.
     */
    public function asBool(string|int ...$path): ?bool;

    /**
     * Try to get the value as `bool`.
     *
     * @throws MismatchedType   If the path is found but is no `bool`.
     * @throws PathDoesNotExist If any key within the path is not found.
     */
    public function tryAsBool(string|int ...$path): bool;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @return bool to indicate if the action was successful or not.
     */
    public function setBool(array $path, bool $value): bool;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @throws PathDoesNotExist To indicate that the given path does not exist.
     * @throws PathTooShort     If the given path is empty.
     */
    public function trySetBool(array $path, bool $value): void;

    // -- Array --

    /**
     * Check if the given path exists and is of type `array`.
     */
    public function isArray(string|int ...$path): bool;

    /**
     * Try to get the value as `array`. Return `null` if there is no `array`.
     *
     * @return ?mixed[]
     */
    public function asArray(string|int ...$path): ?array;

    /**
     * Try to get the value as `array`.
     *
     * @throws MismatchedType   If the path is found but is no `array`.
     * @throws PathDoesNotExist If any key within the path is not found.
     * @return mixed[]
     */
    public function tryAsArray(string|int ...$path): array;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @param mixed[] $value
     * @return bool to indicate if the action was successful or not.
     */
    public function setArray(array $path, array $value): bool;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @param mixed[] $value
     * @throws PathDoesNotExist To indicate that the given path does not exist.
     * @throws PathTooShort     If the given path is empty.
     */
    public function trySetArray(array $path, array $value): void;

    // -- Object --

    /**
     * Check if the given path exists and is of type `object`.
     */
    public function isObject(string|int ...$path): bool;

    /**
     * Try to get the value as `object`. Return `null` if there is no `object`.
     */
    public function asObject(string|int ...$path): ?object;

    /**
     * Try to get the value as `object`.
     *
     * @throws MismatchedType   If the path is found but is no `object`.
     * @throws PathDoesNotExist If any key within the path is not found.
     */
    public function tryAsObject(string|int ...$path): object;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @return bool to indicate if the action was successful or not.
     */
    public function setObject(array $path, object $value): bool;

    /**
     * Set the `$value` at the given `$path`.
     *
     * This does not create new arrays or objects to allow recursively creating new data.
     *
     * @param array<string|int> $path
     * @throws PathDoesNotExist To indicate that the given path does not exist.
     * @throws PathTooShort     If the given path is empty.
     */
    public function trySetObject(array $path, object $value): void;
}
